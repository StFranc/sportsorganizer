/*jshint esversion: 6 */
// filters.js

app.filter('teamsFilter', () => {
    'use strict';
    return (teams, filteringOptions) => {
        return (teams || []).filter((team) => {

            // Filtrowanie drużyn po nazwie
            let result = team.name
                .toLowerCase()
                .includes(filteringOptions.nameFilter.toLowerCase());

            // Filtrowanie po dolnej granicy roku założenia
            if(!isNaN(parseInt(filteringOptions.foundationYearLowerBound)) && result === true){
                result = Number(team.foundationYear) >= filteringOptions.foundationYearLowerBound;
            }

            // Filtrowanie po górnej granicy roku założenia
            if(!isNaN(parseInt(filteringOptions.foundationYearUpperBound)) && result === true){
                result = Number(team.foundationYear) <= filteringOptions.foundationYearUpperBound;
            }
            return result;
        });
    };
});

app.filter('gamesFilter', () => {
    'use strict';
    return (games, filteringOptions) => {
        return (games || []).filter((game) => {
            var result = true;
            // Filtrowanie meczy po drużynie gospodarzy
            if(!isNaN(parseInt(filteringOptions.teams[0].id)) && result === true){
                result = Number(game.teams[0].id) === filteringOptions.teams[0].id;
            }

            // Filtrowanie meczy po drużynie gości
            if(!isNaN(parseInt(filteringOptions.teams[1].id)) && result === true){
                result = Number(game.teams[1].id) === filteringOptions.teams[1].id;
            }
            return result;
        });
    };
});

// Wykonuje przekazaną funkcję na obiektach danej kolecji(w tym przypadku przeznaczone do użycia na drużynach ale można uzywać na dowolnym typie obiektów)
app.filter('addTeamPropertiesFilter', () => {
    'use strict';
    return (teams, addingFunction) => {
        return (teams || []).map((team) => {
            var result = addingFunction(team);
            return result;
        });
    };
});