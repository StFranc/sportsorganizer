/*jshint esversion: 6 */
// gamesController.js

// Kontrolowanie wyświetlania meczy oraz wykonywanie akcji na meczach
app.controller('gamesController', ['$scope', 'sportsService', 'Alertify', function($scope, sportsService, Alertify) {
    'use strict';
    $scope.sharedData = sportsService.data;
    $scope.filteringOptions = {
        teams: [{}, {}]
    };
    // Resetuj zawartość modelu
    ($scope.resetWrite = () => {
        $scope.writeGame = { 
            id: null,
            teams: [{}, {}]
        };
    })();

    // znajdź drużynę o podanym id
    $scope.getTeamById = (id) => {
        return $scope.sharedData.teams.filter(t => t.id === id)[0];
    };

    // wybierz grę do edycji
    $scope.getForEdition = (game) => {
        console.log(game);
        console.log($scope.writeGame);
        $scope.writeGame.id = game.id;
        $scope.writeGame.teams = [
            {id:game.teams[0].id , score:game.teams[0].score },
            {id:game.teams[1].id , score:game.teams[1].score },
        ];
    };

    // Dodaj grę
    $scope.addGame = (game) => {
        var promise = sportsService.addGame(game);
        promise.then((response) =>{
            if(response.data.success) {
                Alertify.success('Mecz pomyślnie dodany!');
                $scope.sharedData.games = response.data.games;
                $scope.resetWrite();
            }
            else {
                Alertify.error('Nie udało się dodać meczu!');
            }
        });
    };

    // Edytuj grę
    $scope.editGame = (game) => {
        var promise = sportsService.editGame(game);
        promise.then((response) =>{
            if(response.data.success) {
                Alertify.success('Mecz pomyślnie zedytowany!');
                $scope.sharedData.teams = response.data.games;
                $scope.resetWrite();
            }
            else {
                Alertify.error('Nie udało się zedytować meczu!');
            }
        });
    };

    // Usuń grę
    $scope.deleteGame = (gameId) => {
        Alertify.confirm('Czy napewno chcesz usunąć mecz?').then(
            function onOk() {
                var promise = sportsService.deleteGame(gameId);
                promise.then((response) =>{
                    if(response.data.success) {
                        Alertify.success('Drużyna pomyślnie usunięta!');

                        // Aktualizuj kolekcje meczy oraz druzyn po stronie klienta
                        $scope.sharedData.games = response.data.games;
                    }
                    else {
                        Alertify.error('Nie udało się usunąć drużyny!');
                    }
                });
            }, 
            function onCancel() { }
        );
    };

}]);