/*jshint esversion: 6 */
// teamsController.js

// Kontrolowanie wyświetlania drużyn oraz wykonywanie akcji na drużynach
app.controller('teamsController', ['$scope', 'sportsService', 'Alertify', function($scope, sportsService, Alertify) {
    'use strict';
    // Udostępnij dane z serwisu w kontrolerze
    $scope.sharedData = sportsService.data;

    // Dodaj drużynę
    $scope.addTeam = (team) => {
        var promise = sportsService.addTeam(team);
        promise.then((response) =>{
            if(response.data.success) {
                Alertify.success('Drużyna pomyślnie dodana!');
                $scope.sharedData.teams = response.data.teams;
                $scope.resetWrite();
            }
            else {
                Alertify.error('Nie udało się dodać drużyny!');
            }
        });
    };

    // Edytuj drużynę
    $scope.editTeam = (team) => {
        var promise = sportsService.editTeam(team);
        promise.then((response) =>{
            if(response.data.success) {
                Alertify.success('Drużyna pomyślnie zedytowana!');
                $scope.sharedData.teams = response.data.teams;
                $scope.resetWrite();
            }
            else {
                Alertify.error('Nie udało się zedytować drużyny!');
            }
        });
    };

    // Usuń drużynę
    $scope.deleteTeam = (teamId) => {
        Alertify.confirm('Czy napewno chcesz usunąć drużynę?').then(
            function onOk() {
                var promise = sportsService.deleteTeam(teamId);
                promise.then((response) =>{
                    if(response.data.success) {
                        Alertify.success('Drużyna pomyślnie usunięta!');

                        // Aktualizuj kolekcje meczy oraz druzyn po stronie klienta
                        $scope.sharedData.teams = response.data.teams;
                        $scope.sharedData.games = response.data.games;
                    }
                    else {
                        Alertify.error('Nie udało się usunąć drużyny!');
                    }
                });
            }, 
            function onCancel() { }
        );
    };

    // Ustaw wstępne opcje filtrowania
    $scope.filteringOptions = {
        nameFilter: '',
        foundationYearLowerBound: null,
        foundationYearUpperBound: null,
    };

    // Ustaw wstępne opcje sortowania
    $scope.sortingOptions = {
        nameSorting: null, // null, 'asc' lub 'desc'
        yearSorting: null, // null, 'asc' lub 'desc'
    };

    // Zainicjalizuj model 
    $scope.writeTeam = {};

    // Resetuj zawartość modelu
    $scope.resetWrite = () => {
        $scope.writeTeam = {};
    };

    // Wybierz drużynę do edycji
    $scope.getForEdition = (team) => {
        $scope.writeTeam.foundationYear = team.foundationYear;
        $scope.writeTeam.name = team.name;
        $scope.writeTeam.id = team.id;
    };

    // Ustawienia sortowania
    $scope.toggleSorting = (property) => {
        if($scope.sortingOptions.property !== property){
            $scope.sortingOptions.property = property;
            $scope.sortingOptions.reverse = false;
        }
        else if($scope.sortingOptions.reverse === true){
            $scope.sortingOptions.reverse = false;
        }
        else if($scope.sortingOptions.reverse === false){
            $scope.sortingOptions.reverse = true;
        }
        else{
            $scope.sortingOptions.property = null;
        }
    };

    // Oblicz punktacje dla drużyny (każda przegrana: 0, remis: 1, wygrana: 3), oblicz ilość bramek straconych i strzelonych
    $scope.calculateTeamStatistics = (team) => {
        let totalScore = 0;
        let scoredGoals = 0;
        let lostGoals = 0;

        $scope.sharedData.games.forEach((g) => {
            // jeśli drużyna grała u siebie
            if(g.teams[0].id === team.id){
                scoredGoals += g.teams[0].score;
                lostGoals += g.teams[1].score;
                if(g.teams[0].score > g.teams[1].score){
                    totalScore += 3;
                }
                else if(g.teams[0].score === g.teams[1].score){
                    totalScore += 1;
                }
            }
            // jeśli drużyna grała na wyjeździe
            if(g.teams[1].id === team.id){
                scoredGoals += g.teams[1].score;
                lostGoals += g.teams[0].score;
                if(g.teams[1].score > g.teams[0].score){
                    totalScore += 3;
                }
                else if(g.teams[1].score === g.teams[0].score){
                    totalScore += 1;
                }
            }
        });
        team.totalScore = totalScore;
        team.scoredGoals = scoredGoals;
        team.lostGoals = lostGoals;
        return team;
    };

    // Maksymalna i minimalna data powstania klubu
    $scope.datesConfig = {
        min: 1800,
        max: (new Date()).getFullYear()
    };
}]);