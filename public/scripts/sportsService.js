/*jshint esversion: 6 */
// sportsService.js

// Komunikacja z serwerem oraz przechowywanie danych dzielonych między kontrolerami
app.service('sportsService',['$http', '$q', function($http, $q){
    'use strict';

    // Dane dzielone przez kontroler drużyn oraz gier
    this.data = {
        teams: [],
        games: []
    };

    // DRUŻYNY

    // Pobieranie meczów z serwera i przypisywanie ich do zmiennej w serwisie
    this.getTeams = function(sharedDataContainer){
        var deferred = $q.defer();
        $http.get('/teams').then(function(data){
            deferred.resolve(data);
        });
        let promise = deferred.promise;
        
        promise.then((response) =>{
            sharedDataContainer.teams = response.data.teams;
        });
    }(this.data);

    // Dodawanie nowej drużyny
    this.addTeam = (team) => {
        var deferred = $q.defer();
        $http.post('/teams', team).then(function(data){
            deferred.resolve(data);
        });
        return deferred.promise;
    };

    // Edytowanie drużyny
    this.editTeam = (team) => {
        var deferred = $q.defer();
        $http.put('/teams', team).then(function(data){
            deferred.resolve(data);
        });
        return deferred.promise;
    };

    // Usuwanie drużyny
    this.deleteTeam = (teamId) => {
        var deferred = $q.defer();
        $http({url: '/teams', method: 'DELETE', data:{ id: teamId}, 
            headers: {
                "Content-Type": "application/json;charset=utf-8"
            }}).then(function(data){
            deferred.resolve(data);
        });
        return deferred.promise;
    };

    // MECZE

    // Pobieranie meczów z serwera i przypisywanie ich do zmiennej w serwisie
    this.getGames = function(sharedData){
        var deferred = $q.defer();
        $http.get('/games').then(function(data){
            deferred.resolve(data);
        });
        let promise = deferred.promise;
        
        promise.then((response) =>{
            sharedData.games = response.data.games;
        });
    }(this.data);

    // Dodawanie nowego meczu
    this.addGame = (game) => {
        var deferred = $q.defer();
        $http.post('/games', game).then(function(data){
            deferred.resolve(data);
        });
        return deferred.promise;
    };

    // Edytowanie meczu
    this.editGame = (game) => {
        var deferred = $q.defer();
        $http.put('/games', game).then(function(data){
            deferred.resolve(data);
        });
        return deferred.promise;
    };

    // Usuwanie meczu
    this.deleteGame = (gameId) => {
        var deferred = $q.defer();
        $http({url: '/games', method: 'DELETE', data:{ id: gameId}, 
            headers: {
                "Content-Type": "application/json;charset=utf-8"
            }}).then(function(data){
            deferred.resolve(data);
        });
        return deferred.promise;
    };
}]);