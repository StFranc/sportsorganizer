# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Setup ###

1. Instalacja Node.js + Node Package Manager
2. Dodanie zmiennej środowiskowej dla folderu z instalacją Node.js
3. $ cd /folder_z_repozytorium
4. $ npm install
5. $ node server.js
6. Wejść na localhost:3000 w przeglądarce

### Dodatkowe informacje ###
Logika serwerowa:
1. cały kod w server.js
2. zawiera konfiguracje serwera jak i listy encji Team i Game:  teamsCollection i gamesCollection które przechowują stan aplikacji na serwerze
3. app.use(...) - konfiguracja middleware, określanie do których statycznych katalogów użytkownik powinien mieć dostęp (np: katalog /public) itp.
4. dodatkowo metody obsługujące zwykłe requesty: app.get(...) - pobieranie danych, app.post(...) - dodawanie nowych rekordów, app.put(...) - modyfikowanie istniejących rekordów oraz app.delete(...) - usuwanie

Logika po stronie klienta:
1. wszystkie pliki w folderze /public/scripts
2. startup.js - uruchomienie aplikacji angularowej
3. kontrolery: teamsController.js i gamesController.js zawierają logikę odpowiedzialną za wyświetlanie danych w tagach oznaczonych kolejno ng-controller="teamsController" i ng-controller="gamesController", nie komunikują się z serverem bezpośrednio a jedynie wywołują metody w sportsService i reagują na zwrócoe "Obietnice" (Promises: https://www.promisejs.org/)
4. zmienna $scope wstrzykiwana do kontrollerów - wszystkie dane przypisane do scope można wyswietlić przy pomocy dyrektyw angularowych w wewnątrz tagu przypisanego danemu kontrolerowi np zmienną $scope.data przypisaną w kontrolerze teamsController:
<div ng-controller="teamsController">
	{{ data }}
</div>
5. ng-model służy do dwustronnego bindowania danych np aby zbindować input do $scope.data:
<input type="text" ng-model="data" />
6. sportsService.js - zawiera funk0cje wysyłające requesty do serwera i zwraca javascriptowe Promises, zawiera dane dzielone między obydwoma kontrolerami (np. kolekcje drużyn i meczów od strony klienta), jest wstrzykiwany do kontrolerów podczas ich tworzenia tak jak zmienna $scope
7. filters.js - filtry drużyn oraz meczów, przykład wykorzystania filtra o nazwie 'teamsFilter' do filtrowania danych w kolekcji $scope.teams:
<tr ng-repeat="t in teams | teamsFilter: dane_dodatkowe">...</tr>