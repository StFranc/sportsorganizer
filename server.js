/*jshint esversion: 6 */
// server.js
'use strict';

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();

// Konfiguracja middleware i dodatkowych bibliotek
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/', express.static(path.join(__dirname, 'public')));
app.use('/bootstrap', express.static(path.join(__dirname, 'node_modules/bootstrap/dist')));
app.use('/angular-bootstrap', express.static(path.join(__dirname, 'node_modules/angular-ui-bootstrap/dist')));
app.use('/jquery', express.static(path.join(__dirname, 'node_modules/jquery/dist')));
app.use('/angular', express.static(path.join(__dirname, 'node_modules/angular')));
app.use('/angular-animate', express.static(path.join(__dirname, 'node_modules/angular-animate')));
app.use('/angular-touch', express.static(path.join(__dirname, 'node_modules/angular-touch')));
app.use('/font-awesome', express.static(path.join(__dirname, 'node_modules/font-awesome/css')));
app.use('/ng-alertify', express.static(path.join(__dirname, 'node_modules/ng-alertify/dist')));

// Pobieranie Strony Głównej
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

// Lista drużyn
var teamsCollection = [
    { id: 1, name: 'Drużyna 1', foundationYear: 1994 }, 
    { id: 2, name: 'Drużyna 2', foundationYear: 2001 }, 
    { id: 3, name: 'Drużyna 3', foundationYear: 1988 }, 
];

// Lista meczy
var gamesCollection = [
    { id: 1, teams: [{ id: 1, score: 3}, {id: 2, score: 1}] }, 
    { id: 2, teams: [{ id: 2, score: 0}, {id: 1, score: 0}] }, 
    { id: 3, teams: [{ id: 3, score: 2}, {id: 2, score: 2}] }, 
    { id: 4, teams: [{ id: 3, score: 1}, {id: 1, score: 0}] }
];

// ZARZĄDZANIE DRUŻYNAMI

// Pobieranie
app.get('/teams', (req, res) => {
    res.json({ teams: teamsCollection });
});

// Dodawanie
app.post('/teams', (req, res) => {
    var result = null;
    var requestTeam = req.body;
    
    // Porażka
    if( requestTeam == undefined || isNullOrEmpty(requestTeam.name) || isNaN(requestTeam.foundationYear)){
        result = { success: false };
    }
    // Sukces
    else{
        var newTeam = {};
        newTeam.id = (teamsCollection
            .map(a => a.id)
            .reduce((a, b) => Math.max(a, b), 0) + 1) || 1;
        newTeam.name = requestTeam.name;
        newTeam.foundationYear = requestTeam.foundationYear;
        teamsCollection.push(newTeam);

        result = {teams: teamsCollection, success: true};
    }
    res.json(result);
});

// Edytowanie
app.put('/teams', (req, res) => {
    var result = null;
    var editedTeam = teamsCollection.filter(t => t.id === req.body.id)[0];

    // Porażka
    if( editedTeam == undefined || isNullOrEmpty(editedTeam.name) || isNaN(editedTeam.foundationYear)){
        result = { success: false };
    }
    // Sukces
    else{
        editedTeam.name = req.body.name;
        editedTeam.foundationYear = req.body.foundationYear;
        result = { teams: teamsCollection, success: true };
    }

    res.json(result);
});

// Usuwanie
app.delete('/teams', (req, res) => {
    var result = null;
    var idToDelete = req.body.id;
    var deletedTeam = teamsCollection.filter(t => t.id === idToDelete)[0];

    // Porażka
    if(deletedTeam == undefined){
        result = { success: false };
    }
    // Sukces
    else{
        // Usuń drużynę
        teamsCollection.splice(teamsCollection.indexOf(deletedTeam), 1);

        // Usuń mecze powiązane z drużyną
        for(let i = 0; i < gamesCollection.length; i++){
            if(gamesCollection[i].teams[0].id === idToDelete || gamesCollection[i].teams[1].id === idToDelete){
                gamesCollection.splice(i--, 1);
            }
        }

        result = {teams: teamsCollection, games: gamesCollection, success: true};
    }

    res.json(result);
});

// ZARZĄDZANIE MECZAMI

// Pobieranie
app.get('/games', (req, res) => {
    res.json({ games: gamesCollection });
});

// Dodawanie
app.post('/games', (req, res) => {
    var result = null;
    var requestGame = req.body;
    
    // Porażka
    if( requestGame == undefined ||
        isNaN(requestGame.teams[0].id) ||
        isNaN(requestGame.teams[0].score) ||
        isNaN(requestGame.teams[1].id) ||
        isNaN(requestGame.teams[1].score)){
        result = { success: false };
    }
    // Sukces
    else{
        var newGame = {};
        newGame.id = (gamesCollection
            .map(a => a.id)
            .reduce((a, b) => Math.max(a, b), 0) + 1) || 1;
        newGame.teams = requestGame.teams;
        newGame.foundationYear = requestGame.foundationYear;
        gamesCollection.push(newGame);

        result = {games: gamesCollection, success: true};
    }
    res.json(result);
});

// Edytowanie
app.put('/games', (req, res) => {
    var result = null;
    var editedGame = gamesCollection.filter(g => g.id === req.body.id)[0];

    // Porażka
    if( editedGame == undefined ||
        isNaN(editedGame.teams[0].score) ||
        isNaN(editedGame.teams[1].score) ||
        editedGame.teams[0].id == undefined ||
        editedGame.teams[1].id == undefined){
        result = { success: false };
    }
    // Sukces
    else{
        editedGame.teams[0].id = req.body.teams[0].id;
        editedGame.teams[0].score = req.body.teams[0].score;
        editedGame.teams[1].id = req.body.teams[1].id;
        editedGame.teams[1].score = req.body.teams[1].score;
        editedGame.foundationYear = req.body.foundationYear;
        result = { teams: teamsCollection, success: true };
    }

    res.json(result);
});

// Usuwanie
app.delete('/games', (req, res) => {
    var result = null;
    var idToDelete = req.body.id;
    var deletedGame = gamesCollection.filter(g => g.id === idToDelete)[0];

    // Porażka
    if(deletedGame == undefined){
        result = { success: false };
    }
    // Sukces
    else{
        // Usuń mecz
        gamesCollection.splice(gamesCollection.indexOf(deletedGame), 1);
        result = { games: gamesCollection, success: true };
    }

    res.json(result);
});

// METODY POMOCNICZE

// Sprawdź czy string = ''/null/undefined
var isNullOrEmpty = (str) => {
    var result = false;
    if(str == null || str.trim() === ""){
        result = true;
    }
    return result;
};

// Uruchomienie aplikacji
app.listen(3000, () => {
    console.log('Application listening on port 3000, press Ctr-C to terminate.');
});